<?php

if ( ! function_exists('tooltip')) {

    function tooltip($message)
    {
        $tooltip = "<a href='#' data-toggle='tooltip' title='" . htmlentities($message) . "'>" . glyphicon("info-sign") . "</a>";
        return $tooltip;
    }

}

if ( ! function_exists('glyphicon')) {

    function glyphicon($icon, $class = '')
    {
        return "<span class='glyphicon glyphicon-{$icon} {$class}' aria-hidden='true'></span>";
    }

}
if ( ! function_exists('alert')) {

    /**
     *
     * @param type $mensaje
     * @param type $tipo
     * @param type $addClass
     * @return type
     */
    function alert($mensaje, $tipo = 'info', $addClass = '')
    {
        $encoding = mb_detect_encoding($mensaje);
        if ($encoding !== "UTF-8") {
            $mensaje = utf8_encode($mensaje);
        }
        return "<div class='alert alert-{$tipo} {$addClass}' >" . $mensaje . "</div>";
    }

}
