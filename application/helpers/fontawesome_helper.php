<?php

if ( ! function_exists('fa')) {

    /**
     *
     * @param string $icon la class del icon
     * @param string $size [lg | 2x | 3x | 4x | 5x] El tama�o aplicar
     * @see http://fontawesome.io/examples/
     * @return type
     */
    function fa($icon, $size = '')
    {
        return "<i class='fa fa-{$icon} fa-{$size}'></i>";
    }

}